class Game {
    constructor() {
        this.p1 = new RandomPlayer();
        this.p2 = new RandomPlayer();
        this.id = this.p1.getName() + "-vs-" + this.p2.getName() + "-" + Date();
    }
    play() {
        this.board = new Board();
        let res = 1;
        let win = ".";
        while (res > 0) {
            res = this.board.move(this.p1.getMove(this.board.board, "0"), "0");
            if (res < 1)
                break;
            win = this.board.checkForWin();
            if (win != ".")
                break;
            res = this.board.move(this.p2.getMove(this.board.board, "1"), "1");
            if (res < 1)
                break;
            win = this.board.checkForWin();
            if (win != ".")
                break;
            this.board.dump();
        }
        if (res < 1) {
            console.log("illegal game state **");
        }
        else {
            console.log(`we should have a winner ${win}`);
        }
    }
    playerFactory() {
        return new RandomPlayer();
    }
}
new Game().play();
//# sourceMappingURL=Game.js.map