var c4;
(function (c4) {
    class Board {
        constructor() {
            this.COLS = 7;
            this.ROWS = 6;
            this.board = [];
            for (let col = 0; col < this.COLS; col++) {
                this.board[col] = [];
                for (let row = 0; row < this.ROWS; row++) {
                    this.board[col][row] = 0;
                }
            }
        }
        dump() {
            console.log("dumping->");
            for (let row = 0; row < this.ROWS; row++) {
                let line = "";
                for (let col = 0; col < this.COLS; col++) {
                    line += this.board[col][this.ROWS - (row + 1)];
                }
                console.log(line);
            }
            console.log("-------");
        }
        isMoveLegal(col) {
            console.log(`col is ${col}`);
            return this.board[col][5] == 0;
        }
        move(col, player) {
            for (let row = 0; row < this.ROWS; row++) {
                if (this.board[col][row] == 0) {
                    this.board[col][row] = player;
                    return row;
                }
            }
            return -1;
        }
        checkForWin() {
            for (let row = 0; row < this.ROWS; row++) {
                let streak = 0;
                let player = -1;
                for (let col = 0; col < this.COLS; col++) {
                    if (this.board[col][row] == 0) {
                        streak = 0;
                        player = -1;
                    }
                    else {
                        if (this.board[col][row] == player) {
                            streak++;
                            if (streak > 3) {
                                return player;
                            }
                        }
                        else {
                            player = this.board[col][row];
                            streak = 1;
                        }
                    }
                }
            }
            for (let col = 0; col < this.COLS; col++) {
                let streak = 0;
                let player = -1;
                for (let row = 0; row < this.ROWS; row++) {
                    if (this.board[col][row] == 0) {
                        streak = 0;
                        player = -1;
                    }
                    else {
                        if (this.board[col][row] == player) {
                            streak++;
                            if (streak > 3) {
                                return player;
                            }
                        }
                        else {
                            player = this.board[col][row];
                            streak = 1;
                        }
                    }
                }
            }
            for (let col = 0; col < this.COLS; col++) {
                let streak = 0;
                let player = -1;
                for (let row = 0; row < this.ROWS; row++) {
                    player = this.board[col][row];
                    if (player != 0) {
                        streak = 1;
                        let x = 0;
                        let dcne = col + 1; // diagonal North East
                        let drne = row + 1;
                        while (dcne + x < this.COLS && drne + x < this.ROWS) {
                            if (this.board[dcne + x][drne + x] == 0) {
                                break;
                            }
                            if (this.board[dcne + x][drne + x] == player) {
                                streak++;
                                if (streak > 3) {
                                    return player;
                                }
                            }
                            else {
                                player = this.board[dcne + x][drne + x];
                                streak = 1;
                            }
                            x++;
                        }
                        player = this.board[col][row];
                        streak = 1;
                        x = 0;
                        let dcnw = col - 1;
                        let drnw = row + 1;
                        while (dcnw - x >= 0 && drnw + x < this.ROWS) {
                            if (this.board[dcnw - x][drnw + x] == 0) {
                                break;
                            }
                            if (this.board[dcnw - x][drnw + x] == player) {
                                streak++;
                                if (streak > 3) {
                                    return player;
                                }
                            }
                            else {
                                player = this.board[dcnw - x][drnw + x];
                                streak = 1;
                            }
                            x++;
                        }
                    }
                }
            }
            return 0;
        }
    }
    c4.Board = Board;
    function testBoard() {
        let tb = new Board();
        let col = [4, 1, 2, 3, 4, 1, 2, 3, 4, 1, 2, 3, 4, 1, 2, 3];
        let player = [1, 2, 1, 2, 1, 1, 1, 1, 1, 2, 1, 1, 2, 2, 2, 2];
        tb.dump();
        for (let x = 0; x < col.length; x++) {
            tb.move(col[x], player[x]);
        }
        tb.dump();
        console.log(`check for win - ${tb.checkForWin()}`);
    }
})(c4 || (c4 = {})); // end module
class SVGBoard {
    constructor() {
        this.colors = ["white", "red", "yellow"];
        let xpad = 15;
        let ypad = 12;
        let radius = 20;
        let w = xpad + (2 * radius + xpad) * 7;
        let h = ypad + (2 * radius + ypad) * 6;
        console.log("Constructing..");
        this.x = 0;
        this.ns = "http://www.w3.org/2000/svg";
        this.canvas = document.getElementById('board');
        let rect = document.createElementNS(this.ns, "rect");
        this.setAttributes(rect, { 'x': '0', 'y': '0', 'width': w, 'rx': '5', 'ry': "5", 'height': h, 'fill': '#3E68DF' });
        console.log(`rect =${rect}`);
        this.canvas.appendChild(rect);
        for (let x = 0; x < 7; x++) {
            for (let y = 0; y < 6; y++) {
                let circ = document.createElementNS(this.ns, "circle");
                this.setAttributes(circ, { 'id': "" + x * 6 + y, 'cx': xpad + radius + (xpad + 2 * radius) * x, 'cy': ypad + radius + (ypad + 2 * radius) * y, 'r': radius, 'fill': '#FFFFFF' });
                this.canvas.appendChild(circ);
            }
        }
        this.board = this.getTestBoard();
    }
    setAttributes(obj, attrs) {
        for (var a in attrs) {
            obj.setAttribute(a, attrs[a]);
        }
    }
    start() {
        console.log("Starting..");
        //this.life.drawGridOnCanvasContext(this.context);
        this.redraw();
    }
    setBoard(newBoard) {
        this.board = newBoard;
    }
    setWinner(player) {
        let p = document.getElementById('result');
        p.innerHTML = `winner = ${this.colors[player]}!`;
    }
    getTestBoard() {
        let m = {
            id: "Test1",
            playe11: "name1",
            player2: "name2",
            me: "player1",
            turn: "player1",
            moves: [
                0, 1, 0, 1, 2, 2, 2, 3
            ],
            board: [
                [1, 1, 1, 1, 1, 1],
                [1, 0, 0, 0, 0, 1],
                [1, 1, 0, 0, 1, 1],
                [0, 0, 0, 0, 0, 0],
                [0, 0, 0, 2, 2, 2],
                [0, 0, 0, 2, 0, 0],
                [2, 2, 2, 2, 2, 2],
            ],
            winner: "-"
        };
        return m.board;
    }
    /**
     * looks at the history of the entire game, and animates it with one move per
     * millisecond interval
     * */
    animateGame(jsonMessage, millisecond) {
    }
    displayBoard(jsonMessage) {
    }
    redraw() {
        for (let x = 0; x < 7; x++) {
            for (let y = 0; y < 6; y++) {
                let element = document.getElementById("" + x * 6 + y);
                element.setAttribute("fill", this.colors[this.board[x][5 - y]]);
            }
        }
    }
}
new SVGBoard().redraw();
//new Canvas().test();
System.register("DumbRemotePlayer", ["express"], function (exports_1, context_1) {
    "use strict";
    var express, cors, DumbRemotePlayer, port, player;
    var __moduleName = context_1 && context_1.id;
    return {
        setters: [
            function (express_1) {
                express = express_1;
            }
        ],
        execute: function () {
            //import * as cors from 'cors'
            cors = require('cors');
            DumbRemotePlayer = class DumbRemotePlayer {
                constructor() {
                    this.name = "Robodummy";
                    this.desc = "Super Simple Remote Player";
                    this.express = express();
                    this.express.use(express.json());
                    this.express.use(cors());
                    this.mountRoutes();
                }
                mountRoutes() {
                    this.express.options(function (req, res, next) {
                        console.log("Options called with no path.. ");
                    });
                    // can ping at any time. 
                    this.express.post('/ping', (req, res, next) => {
                        res.json({
                            player_id: this.name,
                            descr: this.desc
                        });
                        next();
                    });
                    // starts a game, passes game ID 
                    this.express.post('/init', (req, res, next) => {
                        console.log("Arrrr, me be inited");
                        res.json(this.init(req.body));
                        next();
                    });
                    this.express.post('/move', (req, res) => {
                        // decode the req. here, there will be quite a lot of stuff there. 
                        console.log(`Move message received: Body is ${req.body.board}`);
                        let mv = this.move(req.body.board);
                        console.log(`request is ${Object.keys(req.body.board)}`);
                        res.json({
                            player: this.name,
                            move: mv
                        });
                    });
                    this.express.post('/gameover', (req, res, next) => {
                        console.log("Aw, gameover, I was enjoying that");
                        res.json({ player_id: this.name, note: this.desc });
                        next();
                    });
                }
                init(messageObject) {
                    this.game_id = messageObject["game_id"];
                    this.which_player = messageObject["which_player"];
                    console.log(`Message received By DumbRemotePlayer is: ${messageObject["message"]}`);
                    return { player_id: this.name, descr: this.desc };
                }
                move(board) {
                    let attempt = Math.floor(Math.random() * 7);
                    for (let i = 0; i < 7; i++) {
                        for (let r = 0; r < board[attempt].length; r++) {
                            if (board[attempt][r] == 0) {
                                return attempt;
                            }
                        }
                        attempt = (attempt + 1) % 7;
                    }
                    return -1; // error..
                }
                startServer(port) {
                    this.express.listen(port, (err) => {
                        if (err) {
                            return console.log(err);
                        }
                        return console.log(`DumbRemotePlayer is listening on ${port}`);
                    });
                }
            };
            port = process.env.PORT || 3000;
            player = new DumbRemotePlayer();
            player.startServer(3000);
        }
    };
});
var c4;
(function (c4) {
    class Game {
        constructor(p1, p2) {
            this.canvas = new SVGBoard();
            this.game_ID = Date();
            this.board = new c4.Board();
            this.gameover = false;
            this.turns = 0;
            this.current = 1;
            this.winner = 0;
            this.players = [];
            this.players[0] = null;
            this.players[1] = p1;
            this.players[2] = p2;
            this.canvas.start();
        }
        // a kind of state machine.
        // messages here:
        pingRequest(player) {
            let message = {};
            this.sendMessage(player, "ping", message, "pingResponse");
        }
        pingResponse(player, result) {
            this.players[player].pinged = true;
            this.gamePlay();
        }
        initRequest(player) {
            let msg = { game_id: this.game_ID, which_player: player, message: "some message about the game.. " };
            this.sendMessage(player, "init", msg, "initResponse");
        }
        initResponse(player, result) {
            this.players[player].initalized = true;
            console.log(`** Player ${player} initialized. Message - ${result.player_id}`);
            this.gamePlay();
        }
        moveRequest(player) {
            let msg = {
                id: this.game_ID,
                me: this.players[player].player_id,
                opponent: this.players[player % 2 + 1].player_id,
                player_no: player,
                turns: this.turns,
                moves: this.moves,
                board: this.board.board,
                error: "",
                winner: 0
            };
            this.sendMessage(player, "move", msg, "moveResponse");
        }
        moveResponse(player, result) {
            let move = result.move;
            console.log(`And the move is ${move} `);
            if (!this.board.isMoveLegal(move)) {
                // send error message to last player.
                // send end game message to all players with note.. 
            }
            // this is where we do the players moves, check for winner etc. 
            this.board.move(move, player);
            this.canvas.setBoard(this.board.board);
            this.canvas.redraw();
            this.winner = this.board.checkForWin();
            this.turns += 1;
            this.nextPlayer();
            setTimeout(this.gamePlay.bind(this), 1000);
        }
        gameoverRequest(player) {
            console.log(`player is ${player}`);
            let msg = { winner: this.winner,
                turns: this.turns,
                board: this.board.board
            };
            this.sendMessage(player, "gameover", msg, "gameoverResponse");
        }
        gameoverResponse(player, result) {
            console.log(`acknowledgement ${result.player_id} `);
            this.players[player].finished = true;
            this.gamePlay();
        }
        gamePlay() {
            if (!this.players[1].pinged) {
                this.pingRequest(1);
            }
            else if (!this.players[2].pinged) {
                this.pingRequest(2);
            }
            else if (!this.players[1].initalized) {
                this.initRequest(1);
            }
            else if (!this.players[2].initalized) {
                this.initRequest(2);
            }
            else if (this.board.checkForWin() == 0) {
                this.moveRequest(this.current);
            }
            else if (!this.players[1].finished) {
                this.gameoverRequest(1);
            }
            else if (!this.players[2].finished) {
                this.gameoverRequest(2);
            }
            else {
                console.log("Looks like the game is over.");
                console.log(`Winner seems to be ${this.winner}`);
                this.board.dump();
                this.canvas.setWinner(this.winner);
            }
        }
        nextPlayer() {
            this.current = (this.current % 2) + 1;
        }
        end(g) {
            console.log("Ya. We are done. ");
        }
        sendMessage(player, command, message, next) {
            console.log(`player is ${this.players[player]}`);
            let jm = JSON.stringify(message);
            let myRequest = new XMLHttpRequest();
            let prt = this.players[player].port;
            let playerURL = "";
            if (prt) {
                playerURL = this.players[player].url + ":" + prt + "/" + command;
            }
            else {
                playerURL = this.players[player].url + "/" + command;
            }
            console.log(`URL requested is: ${playerURL}`);
            let that = this;
            myRequest.open('POST', playerURL);
            myRequest.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
            myRequest.setRequestHeader("Airplane", "application/json;charset=UTF-8");
            myRequest.onreadystatechange = function () {
                if (myRequest.readyState === 4) {
                    let res = JSON.parse(myRequest.responseText);
                    that[next](player, res);
                }
            };
            myRequest.send(jm);
        }
    }
    c4.Game = Game;
})(c4 || (c4 = {}));
var c4;
(function (c4) {
    class PlayerProxy {
        constructor(url, port) {
            let s1 = ["silky", "big", "tiny", "evil", "shiny", "bad", "goodly", "annoying", "scary", "warty"];
            let s2 = ["glowing", "running", "fierce", "friendly", "jumping", "creeping", "flying", "hopping", "leaping", "stealthy", "Mc"];
            let s3 = ["cat", "hedgehog", "dwarf", "goblin", "wizard", "mermaid", "wolf", "tiger", "lion", "serpent", "hero", "bat", "hog"];
            this.player_id = s1[Math.floor(Math.random() * s1.length)]
                + "-" + s2[Math.floor(Math.random() * s2.length)]
                + "-" + s3[Math.floor(Math.random() * s3.length)];
            let move = Math.floor(Math.random() * 7);
            this.url = url;
            this.port = port;
            // construct end point. Load config file or something. 
        }
        ping() {
        }
        init(game_ID, which) {
            let j = { cock: "melud" };
            //let res = this.httpRequestResponse(this.endpoint,this.port,"ping",j)
            // if ping isn't good, error here.. 
            //this.game_ID = game_ID
            //let k = {game_id : game_ID, which_player:which, message: "Init.."}
            //res = this.httpRequestResponse(this.endpoint,this.port,"init",k)
            //console.log(`response from init message is: ${res.player_id}`)
            //if (this.player_id != res.player_id) {
            //    throw Error("Inconsistency in Ping / Init") 
            //}   
            //this.descr = res.descr
        }
        getMove(board, me, turn, vs, moves, winner) {
            // construct message. 
            let players = [];
            players[me - 1] = this.player_id; // me-1 converts the player no. to an array index. 
            players[Math.abs((me - 2) % 2)] = vs;
            let message = {
                "id": this.game_ID,
                "players": players,
                "me": this.player_id,
                "turn": turn,
                "moves": moves,
                "board": board,
                "error": "",
                "winner": winner
            };
            // send message.. 
            return 0;
        }
        endGame(result) {
        }
    }
    c4.PlayerProxy = PlayerProxy;
})(c4 || (c4 = {})); // end module
var c4;
(function (c4) {
    function play_game(url1, port1, url2, port2) {
        if (!url1) {
            url1 = "http://localhost";
            port1 = 3000;
        }
        if (!url2) {
            url2 = "http://localhost";
            port2 = 3000;
        }
        let g = new c4.Game(new c4.PlayerProxy(url1, port1), new c4.PlayerProxy(url2, port2));
        g.gamePlay();
        console.log("done.. like as in the end. done.");
    }
    c4.play_game = play_game;
    console.log("JS Loaded");
})(c4 || (c4 = {}));
//------
//testGame()
//testPlayer()
//testRemotePlayer()
// Notes to self:  What we really need is a "game ticker" - a function that keeps 
// calling itself, and progressing state.. 
//# sourceMappingURL=bundle.js.map