class Board {
    constructor() {
        this.COLS = 7;
        this.ROWS = 6;
        this.board = [];
        for (let col = 0; col < this.COLS; col++) {
            this.board[col] = [];
            for (let row = 0; row < this.ROWS; row++) {
                this.board[col][row] = 0;
            }
        }
    }
    dump() {
        console.log("dumping->");
        for (let row = 0; row < this.ROWS; row++) {
            let line = "";
            for (let col = 0; col < this.COLS; col++) {
                line += this.board[col][this.ROWS - (row + 1)];
            }
            console.log(line);
        }
        console.log("-------");
    }
    isMoveLegal(col) {
        console.log(`col is ${col}`);
        return this.board[col][5] == 0;
    }
    move(col, player) {
        for (let row = 0; row < this.ROWS; row++) {
            if (this.board[col][row] == 0) {
                this.board[col][row] = player;
                return row;
            }
        }
        return -1;
    }
    checkForWin() {
        for (let row = 0; row < this.ROWS; row++) {
            let streak = 0;
            let player = -1;
            for (let col = 0; col < this.COLS; col++) {
                if (this.board[col][row] == 0) {
                    streak = 0;
                    player = -1;
                }
                else {
                    if (this.board[col][row] == player) {
                        streak++;
                        if (streak > 3) {
                            return player;
                        }
                    }
                    else {
                        player = this.board[col][row];
                        streak = 1;
                    }
                }
            }
        }
        for (let col = 0; col < this.COLS; col++) {
            let streak = 0;
            let player = -1;
            for (let row = 0; row < this.ROWS; row++) {
                if (this.board[col][row] == 0) {
                    streak = 0;
                    player = -1;
                }
                else {
                    if (this.board[col][row] == player) {
                        streak++;
                        if (streak > 3) {
                            return player;
                        }
                    }
                    else {
                        player = this.board[col][row];
                        streak = 1;
                    }
                }
            }
        }
        for (let col = 0; col < this.COLS; col++) {
            let streak = 0;
            let player = -1;
            for (let row = 0; row < this.ROWS; row++) {
                player = this.board[col][row];
                if (player != 0) {
                    streak = 1;
                    let x = 0;
                    let dcne = col + 1; // diagonal North East
                    let drne = row + 1;
                    while (dcne + x < this.COLS && drne + x < this.ROWS) {
                        if (this.board[dcne + x][drne + x] == 0) {
                            break;
                        }
                        if (this.board[dcne + x][drne + x] == player) {
                            streak++;
                            if (streak > 3) {
                                return player;
                            }
                        }
                        else {
                            player = this.board[dcne + x][drne + x];
                            streak = 1;
                        }
                        x++;
                    }
                    player = this.board[col][row];
                    streak = 1;
                    x = 0;
                    let dcnw = col - 1;
                    let drnw = row + 1;
                    while (dcnw - x >= 0 && drnw + x < this.ROWS) {
                        if (this.board[dcnw - x][drnw + x] == 0) {
                            break;
                        }
                        if (this.board[dcnw - x][drnw + x] == player) {
                            streak++;
                            if (streak > 3) {
                                return player;
                            }
                        }
                        else {
                            player = this.board[dcnw - x][drnw + x];
                            streak = 1;
                        }
                        x++;
                    }
                }
            }
        }
        return 0;
    }
}
function testBoard() {
    let tb = new Board();
    let col = [4, 1, 2, 3, 4, 1, 2, 3, 4, 1, 2, 3, 4, 1, 2, 3];
    let player = [1, 2, 1, 2, 1, 1, 1, 1, 1, 2, 1, 1, 2, 2, 2, 2];
    tb.dump();
    for (let x = 0; x < col.length; x++) {
        tb.move(col[x], player[x]);
    }
    tb.dump();
    console.log(`check for win - ${tb.checkForWin()}`);
}
//# sourceMappingURL=Connect4.js.map
class Player {
    init(game_ID, which) { }
    getID() { return ""; }
    getMove(board, me, turn, vs, moves, winner) { return 0; }
    endGame(result) { }
}
class RandomPlayer extends Player {
    constructor() {
        super();
        this.name = "";
        this.s1 = ["silky", "big", "tiny", "evil", "shiny", "bad", "goodly", "annoying", "scary", "warty"];
        this.s2 = ["glowing", "running", "fierce", "friendly", "jumping", "creeping", "flying", "hopping", "leaping", "stealthy"];
        this.s3 = ["cat", "hedgehog", "dwarf", "goblin", "wizard", "mermaid", "wolf", "tiger", "lion", "serpent", "hero", "bat", "hog"];
        this.name = this.s1[Math.floor(Math.random() * this.s1.length)]
            + "-" + this.s2[Math.floor(Math.random() * this.s2.length)]
            + "-" + this.s3[Math.floor(Math.random() * this.s3.length)];
        let move = Math.floor(Math.random() * 7);
    }
    init(game_ID, which) {
        this.whichPlayerAmI = which;
    }
    getID() {
        return this.name;
    }
    getMove(board, me, turn, vs, moves, winner) {
        let move = Math.floor(Math.random() * 7);
        while (!board.isMoveLegal(move)) {
            move = (move + 1) % 5;
        }
        return move;
    }
    endGame(result) {
        let r = (result == this.name) ? "won" : "lost";
        console.log(`${this.name} ${r}`);
    }
}
function testPlayer() {
    let p = new RandomPlayer();
    console.log(p.getID());
    let b = new Board();
    for (let x = 0; x < 35; x++) {
        let m = p.getMove(b, 1, 0, "Test Opponent", [0], 0);
        b.move(m, (x % 2) + 1);
    }
    b.dump();
}
class InitMessage {
    constructor(id, which, message) {
        this.game_id = id;
        this.which_player = which;
        this.message = message;
    }
}
class BoardMessage {
}
class RemotePlayerProxy extends Player {
    constructor() {
        super();
        this.url = "http://localhost";
        this.port = 3000;
        // construct end point. Load config file or something. 
    }
    ping() {
    }
    init(game_ID, which) {
        let j = { cock: "melud" };
        //let res = this.httpRequestResponse(this.endpoint,this.port,"ping",j)
        // if ping isn't good, error here.. 
        //this.game_ID = game_ID
        //let k = {game_id : game_ID, which_player:which, message: "Init.."}
        //res = this.httpRequestResponse(this.endpoint,this.port,"init",k)
        //console.log(`response from init message is: ${res.player_id}`)
        //if (this.player_id != res.player_id) {
        //    throw Error("Inconsistency in Ping / Init") 
        //}   
        //this.descr = res.descr
    }
    getMove(board, me, turn, vs, moves, winner) {
        // construct message. 
        let players = [];
        players[me - 1] = this.player_id; // me-1 converts the player no. to an array index. 
        players[Math.abs((me - 2) % 2)] = vs;
        let message = {
            "id": this.game_ID,
            "players": players,
            "me": this.player_id,
            "turn": turn,
            "moves": moves,
            "board": board,
            "error": "",
            "winner": winner
        };
        // send message.. 
        return 0;
    }
    endGame(result) {
    }
}
/**
 * a lightweight game object, stores game state.
 */
class Game {
    constructor(p1, p2) {
        this.canvas = new Canvas();
        this.game_ID = Date();
        this.board = new Board();
        this.gameover = false;
        this.turns = 0;
        this.current = 1;
        this.winner = 0;
        this.players = [];
        this.players[0] = null;
        this.players[1] = p1;
        this.players[2] = p2;
        this.canvas.start();
    }
    // a kind of state machine.
    // messages here:
    pingRequest(player) {
        let message = {};
        this.sendMessage(player, "ping", message, "pingResponse");
    }
    pingResponse(player, result) {
        this.players[player].pinged = true;
        this.gamePlay();
    }
    initRequest(player) {
        let msg = { game_id: this.game_ID, which_player: player, message: "some message about the game.. " };
        this.sendMessage(player, "init", msg, "initResponse");
    }
    initResponse(player, result) {
        this.players[player].initalized = true;
        console.log(`** Player ${player} initialized. Message - ${result.player_id}`);
        this.gamePlay();
    }
    moveRequest(player) {
        let msg = {
            id: this.game_ID,
            me: this.players[player].player_id,
            opponent: this.players[player % 2 + 1].player_id,
            player_no: player,
            turns: this.turns,
            moves: this.moves,
            board: this.board.board,
            error: "",
            winner: 0
        };
        this.sendMessage(player, "move", msg, "moveResponse");
    }
    moveResponse(player, result) {
        let move = result.move;
        console.log(`And the move is ${move} `);
        if (!this.board.isMoveLegal(move)) {
            // send error message to last player.
            // send end game message to all players with note.. 
        }
        // this is where we do the players moves, check for winner etc. 
        this.board.move(move, player);
        this.canvas.setBoard(this.board.board);
        this.canvas.redraw();
        this.winner = this.board.checkForWin();
        this.turns += 1;
        this.nextPlayer();
        setTimeout(this.gamePlay.bind(this), 1000);
    }
    gameoverRequest(player) {
        console.log(`player is ${player}`);
        let msg = { winner: this.winner,
            turns: this.turns,
            board: this.board.board
        };
        this.sendMessage(player, "gameover", msg, "gameoverResponse");
    }
    gameoverResponse(player, result) {
        console.log(`acknowledgement ${result.player_id} `);
        this.players[player].finished = true;
        this.gamePlay();
    }
    gamePlay() {
        if (!this.players[1].pinged) {
            this.pingRequest(1);
        }
        else if (!this.players[2].pinged) {
            this.pingRequest(2);
        }
        else if (!this.players[1].initalized) {
            this.initRequest(1);
        }
        else if (!this.players[2].initalized) {
            this.initRequest(2);
        }
        else if (this.board.checkForWin() == 0) {
            this.moveRequest(this.current);
        }
        else if (!this.players[1].finished) {
            this.gameoverRequest(1);
        }
        else if (!this.players[2].finished) {
            this.gameoverRequest(2);
        }
        else {
            console.log("Looks like the game is over.");
            console.log(`Winner seems to be ${this.winner}`);
            this.board.dump();
            this.canvas.setWinner(this.winner);
        }
    }
    nextPlayer() {
        this.current = (this.current % 2) + 1;
    }
    end(g) {
        console.log("Ya. We are done. ");
    }
    sendMessage(player, command, message, next) {
        console.log(`player is ${this.players[player]}`);
        let jm = JSON.stringify(message);
        let myRequest = new XMLHttpRequest();
        let playerURL = this.players[player].url + ":" + this.players[player].port + "/" + command;
        let that = this;
        myRequest.open('POST', playerURL);
        myRequest.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        myRequest.onreadystatechange = function () {
            if (myRequest.readyState === 4) {
                let res = JSON.parse(myRequest.responseText);
                that[next](player, res);
            }
        };
        myRequest.send(jm);
    }
}
function testRemotePlayer() {
    let p = new RemotePlayerProxy();
    p.init("hoohaa", 1);
}
function end(g) {
    console.log("player 2 initialized ");
    console.log(`reponse is ${g.players[2].response}`);
}
function play_game() {
    let g = new Game(new RemotePlayerProxy(), new RemotePlayerProxy());
    g.gamePlay();
    console.log("done.. like as in the end. done.");
}
//------
//testGame()
//testPlayer()
//testRemotePlayer()
// Notes to self:  What we really need is a "game ticker" - a function that keeps 
// calling itself, and progressing state.. 
class Canvas {
    constructor() {
        this.colors = ["white", "red", "yellow"];
        console.log("Constructing..");
        this.x = 0;
        this.canvas = document.getElementById('canvas');
        console.log(`canvas =${this.canvas}`);
        this.context = this.canvas.getContext("2d");
        console.log(`context = ${this.context}`);
        this.context.lineWidth = 1;
        this.piece_size = 80;
        this.spacing = 20;
        this.board = this.getTestBoard();
    }
    start() {
        console.log("Starting..");
        //this.life.drawGridOnCanvasContext(this.context);
        this.redraw();
    }
    setBoard(newBoard) {
        this.board = newBoard;
    }
    setWinner(player) {
        let p = document.getElementById('result');
        p.innerHTML = `winner = ${this.colors[player]}!`;
    }
    getTestBoard() {
        let m = {
            id: "Test1",
            playe11: "name1",
            player2: "name2",
            me: "player1",
            turn: "player1",
            moves: [
                0, 1, 0, 1, 2, 2, 2, 3
            ],
            board: [
                [0, 0, 0, 0, 0, 0],
                [1, 1, 2, 0, 0, 0],
                [2, 0, 0, 0, 0, 0],
                [2, 1, 0, 0, 0, 0],
                [1, 1, 0, 0, 0, 0],
                [2, 0, 0, 0, 0, 0],
                [1, 2, 1, 2, 0, 0],
            ],
            winner: "-"
        };
        return m.board;
    }
    /**
     * looks at the history of the entire game, and animates it with one move per
     * millisecond interval
     * */
    animateGame(jsonMessage, millisecond) {
    }
    displayBoard(jsonMessage) {
    }
    redraw() {
        // column first traversal. 
        this.context.fillStyle = 'blue';
        this.context.fillRect(0, 0, (7 * this.piece_size + 8 * this.spacing), (6 * this.piece_size + 7 * this.spacing));
        this.context.fillStyle = 'white';
        let radius = this.piece_size / 2;
        let cx = 0;
        let cy = 0;
        for (let x = 0; x < 7; x++) {
            cx = (x + 1) * (this.spacing + this.piece_size) - radius;
            for (let y = 0; y < 6; y++) {
                cy = (y + 1) * (this.spacing + this.piece_size) - radius;
                this.context.fillStyle = this.colors[this.board[x][5 - y]];
                this.context.beginPath();
                this.context.arc(cx, cy, radius, 0, 2 * Math.PI);
                this.context.fill();
            }
        }
        //window.requestAnimationFrame(this.redraw.bind(this));
    }
}
new Canvas().start();
//new Canvas().test();
//# sourceMappingURL=out.js.map