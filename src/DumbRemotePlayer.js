"use strict";
exports.__esModule = true;
var express = require("express");
//import * as cors from 'cors'
var cors = require('cors');
//const exp = require('express');
var DumbRemotePlayer = /** @class */ (function () {
    function DumbRemotePlayer() {
        this.name = "Robodummy";
        this.desc = "Super Simple Remote Player";
        this.express = express();
        this.express.use(express.json());
        this.express.use(cors());
        this.mountRoutes();
    }
    DumbRemotePlayer.prototype.mountRoutes = function () {
        var _this = this;
        this.express.options(function (req, res, next) {
            console.log("Options called with no path.. ");
        });
        // this.express.options('/ping', function(req, res, next) {
        //     res.header("Access-Control-Allow-Origin", "*");
        //     res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Airplane");
        //     console.log("spit in my mouth")
        //     res.end()
        //     next();
        //   });
        // this.express.use('/ping',(req, res, next) => {
        //     //res.header("Access-Control-Allow-Origin", "*");
        //     //res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Airplane");
        //     console.log("eat my hand")
        //     res.end()
        //     next();
        //   });
        // can ping at any time. 
        this.express.post('/ping', function (req, res, next) {
            if (req.method === "OPTIONS") {
                console.log("weird, like square chips..");
                return res.status(200).end();
            }
            console.log("Forsooth, I am pinged");
            //res.header("Access-Control-Allow-Origin", "*");
            //res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
            res.json({
                player_id: _this.name,
                descr: _this.desc
            });
            next();
        });
        // starts a game, passes game ID 
        this.express.post('/init', function (req, res, next) {
            console.log("Arrrr, me be inited");
            //res.header("Access-Control-Allow-Origin", "*");
            //res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
            res.json(_this.init(req.body));
            next();
        });
        this.express.post('/move', function (req, res) {
            // decode the req. here, there will be quite a lot of stuff there. 
            console.log("Move message received: Body is " + req.body.board);
            var mv = _this.move(req.body.board);
            console.log("request is " + Object.keys(req.body.board));
            //res.header("Access-Control-Allow-Origin", "*");
            //res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
            res.json({
                player: _this.name,
                move: mv
            });
        });
        this.express.post('/gameover', function (req, res, next) {
            console.log("Aw, gameover, I was enjoying that");
            //res.header("Access-Control-Allow-Origin", "*");
            //res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
            res.json({ player_id: _this.name, note: _this.desc });
            next();
        });
    };
    DumbRemotePlayer.prototype.init = function (messageObject) {
        this.game_id = messageObject["game_id"];
        this.which_player = messageObject["which_player"];
        console.log("Message received By DumbRemotePlayer is: " + messageObject["message"]);
        return { player_id: this.name, descr: this.desc };
    };
    DumbRemotePlayer.prototype.move = function (board) {
        var attempt = Math.floor(Math.random() * 7);
        for (var i = 0; i < 7; i++) {
            for (var r = 0; r < board[attempt].length; r++) {
                if (board[attempt][r] == 0) {
                    return attempt;
                }
            }
            attempt = (attempt + 1) % 7;
        }
        return -1; // error..
    };
    DumbRemotePlayer.prototype.startServer = function (port) {
        this.express.listen(port, function (err) {
            if (err) {
                return console.log(err);
            }
            return console.log("DumbRemotePlayer is listening on " + port);
        });
    };
    return DumbRemotePlayer;
}());
var port = process.env.PORT || 3000;
var player = new DumbRemotePlayer();
player.startServer(3000);
