class SVGBoard {
    private canvas: HTMLElement; // board
    private ns: string
    private x: number;
    private piece_size: number;
    private spacing: number;
    private colors = ["white","red","yellow"]
    board: number[][]

    constructor() {
        let xpad = 15
        let ypad = 12
        let radius = 20
        let w = xpad + (2*radius + xpad ) * 7 
        let h = ypad + (2*radius + ypad ) * 6
        console.log("Constructing..")
        this.x = 0
        this.ns = "http://www.w3.org/2000/svg";
        this.canvas = document.getElementById('board');
        let rect = document.createElementNS(this.ns, "rect");
        this.setAttributes(rect, {'x':'0', 'y':'0', 'width':w, 'rx':'5', 'ry':"5", 'height':h, 'fill':'#3E68DF'})
        console.log(`rect =${rect}`)
        this.canvas.appendChild(rect)

        for (let x=0; x < 7; x++)  {
            for (let y=0; y< 6; y++) {
                let circ = document.createElementNS(this.ns, "circle");
                this.setAttributes(circ, {'id':""+x*6+y, 'cx': xpad + radius + (xpad + 2*radius)*x, 'cy':ypad + radius + (ypad + 2*radius)*y, 'r':radius,'fill':'#FFFFFF'})
                this.canvas.appendChild(circ)
            }
        }        
        this.board = this.getTestBoard()
    }
    setAttributes(obj: any, attrs: any) {
        for(var a in attrs) {
            obj.setAttribute(a, attrs[a])
        }
    }
    start() {
        console.log("Starting..")
        //this.life.drawGridOnCanvasContext(this.context);
        this.redraw()
    }
    setBoard(newBoard: number[][]) {
        this.board = newBoard
    }
    setWinner(player : number) {
        let p = document.getElementById('result')
        p.innerHTML = `winner = ${this.colors[player]}!`
    }
    getTestBoard() : number[][] {
        let m = {
            id: "Test1",
            playe11: "name1",
            player2: "name2",
            me : "player1",
            turn: "player1",
            moves : [ 
                0,1,0,1,2,2,2,3], 
            board: [ 
                [1,1,1,1,1,1], 
                [1,0,0,0,0,1],
                [1,1,0,0,1,1],
                [0,0,0,0,0,0],
                [0,0,0,2,2,2],
                [0,0,0,2,0,0],
                [2,2,2,2,2,2],
            ], 
            winner: "-"
        }
        
        return m.board 
    }
    /**
     * looks at the history of the entire game, and animates it with one move per 
     * millisecond interval
     * */ 
    private animateGame(jsonMessage : string, millisecond : number) {
        
    }
    private displayBoard(jsonMessage : string) {
        
    }
    
    redraw() {
        for (let  x = 0; x < 7; x++) {
            for (let y = 0; y < 6; y++) {
                let element = document.getElementById(""+x*6+y)
                element.setAttribute("fill", this.colors[this.board[x][5-y]]);
            }
        
       } 
    }

}
