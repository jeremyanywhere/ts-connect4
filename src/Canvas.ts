module c4 {
export class Canvas {
    private canvas: HTMLCanvasElement;
    private context: CanvasRenderingContext2D;
    private x: number;
    private piece_size: number;
    private spacing: number;
    private colors = ["white","red","yellow"]
    board: number[][]

    constructor() {
        console.log("Constructing..")
        this.x = 0
        this.canvas = document.getElementById('canvas') as
                 HTMLCanvasElement;
        console.log(`canvas =${this.canvas}`)
        this.context = this.canvas.getContext("2d");
        console.log(`context = ${this.context}`)
        this.context.lineWidth = 1;
        this.piece_size = 80;
        this.spacing = 20;
        this.board = this.getTestBoard()
    }
    start() {
        console.log("Starting..")
        //this.life.drawGridOnCanvasContext(this.context);
        this.redraw()
    }
    setBoard(newBoard: number[][]) {
        this.board = newBoard
    }
    setWinner(player : number) {
        let p = document.getElementById('result')
        p.innerHTML = `winner = ${this.colors[player]}!`
    }
    getTestBoard() : number[][] {
        let m = {
            id: "Test1",
            playe11: "name1",
            player2: "name2",
            me : "player1",
            turn: "player1",
            moves : [ 
                0,1,0,1,2,2,2,3], 
            board: [ 
                [1,1,1,1,1,1], 
                [1,0,0,0,0,1],
                [1,1,0,0,1,1],
                [0,0,0,0,0,0],
                [0,0,0,2,2,2],
                [0,0,0,2,0,0],
                [2,2,2,2,2,2],
            ], 
            winner: "-"
        }
        
        return m.board 
    }
    /**
     * looks at the history of the entire game, and animates it with one move per 
     * millisecond interval
     * */ 
    private animateGame(jsonMessage : string, millisecond : number) {
        
    }
    private displayBoard(jsonMessage : string) {
        
    }
    
    redraw() {
        // column first traversal. 
        this.context.fillStyle = 'blue';
        this.context.fillRect(0,0,(7*this.piece_size + 8*this.spacing),(6*this.piece_size + 7*this.spacing));
        this.context.fillStyle = 'white';
        let radius = this.piece_size /2 ;
        let cx=0
        let cy=0
        for (let  x = 0; x < 7; x++) {
            cx = (x +1) * (this.spacing + this.piece_size) - radius 
            for (let y = 0; y < 6; y++) {
                cy = (y +1) * (this.spacing + this.piece_size) - radius 
                this.context.fillStyle = this.colors[this.board[x][5-y]];
                this.context.beginPath();
                this.context.arc(cx, cy, radius, 0, 2 * Math.PI);
                this.context.fill();
            }
        
       } 
        //window.requestAnimationFrame(this.redraw.bind(this));
    }

}
}//end module
//new Canvas().start();
//new Canvas().test();