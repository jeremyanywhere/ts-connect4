import {Board} from "./Board"
class RandomPlayer {
    whichPlayerAmI : number
    name : string
    s1 : string[]
    s2 : string[]
    s3 : string[]
    constructor() {
        this.name = "";
        this.s1 = ["silky", "big", "tiny", "evil", "shiny", "bad", "goodly", "annoying", "scary", "warty", "little"];
        this.s2 = ["glowing", "running", "fierce", "friendly", "jumping", "creeping", "flying", "hopping", "leaping", "stealthy", "bouncy", "Mc"];
        this.s3 = ["cat", "hedgehog", "dwarf", "goblin", "elf", "wizard", "mermaid", "wolf", "tiger", "lion", "serpent", "hero", "bat", "hog", "witch"];
        this.name = this.s1[Math.floor(Math.random() * this.s1.length)]
            + "-" + this.s2[Math.floor(Math.random() * this.s2.length)]
            + "-" + this.s3[Math.floor(Math.random() * this.s3.length)];
        let move = Math.floor(Math.random() * 7);
    }

    init(game_ID : string, which : number) {
        this.whichPlayerAmI = which
    }
    getID() : string {
        return this.name;
    }
    getMove(board : Board, me: number, turn : number, vs : string, moves : number[], winner : number ) : number {
        let move = Math.floor(Math.random() * 7);
        while (!board.isMoveLegal(move)) {
            move = (move + 1) % 5;
        }
        return move;
    }
    endGame(result : string) {
        let r = (result == this.name)? "won":"lost"
        console.log(`${this.name} ${r}`);
    }
}
function testPlayer() {
    let p = new RandomPlayer();
    console.log(p.getID());
    let b = new Board() 
    for (let x=0; x < 35; x++) {
        let m = p.getMove(b, 1, 0, "Test Opponent", [0], 0)
        b.move(m, (x%2)+1)
    }
    b.dump()
}