const {createServer} = require("http");
let server = createServer((request, response) => {
  response.writeHead(200, {"Content-Type": "text/html"}); 
  response.write(`
     <h1>Hello!</h1>
    <p>You asked for <code>${Object.keys(request.client).length}</code></p>`); response.end();
    //console.log(`headers are ${request.client}`)
    console.log(`request's properties are ${Object.keys(request)}`)
});
server.listen(8000); console.log("Listening! (port 8000)");
