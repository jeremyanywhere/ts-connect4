import * as express from 'express'
//import * as cors from 'cors'
var cors = require('cors');
import { timingSafeEqual } from 'crypto';
import { endianness } from 'os';


class DumbRemotePlayer { 
    public express 
    public name
    public desc
    public game_id: string
    public which_player: string 
    public message: string


    constructor () {
        this.name = "Robodummy"
        this.desc = "Super Simple Remote Player"
        this.express = express()
        this.express.use(express.json())
        this.express.use(cors());
        this.mountRoutes()

    }
    private mountRoutes (): void {
        this.express.options(function(req, res, next) {
          console.log("Options called with no path.. ")  
        })
        // can ping at any time. 
        this.express.post('/ping', (req, res, next) => {
            res.json({
                player_id: this.name,
                descr: this.desc
            })
            next()
        })
        // starts a game, passes game ID 
        this.express.post('/init', (req, res, next) => {
            console.log("Arrrr, me be inited")
           res.json(this.init(req.body))
            next()
        })
        this.express.post('/move', (req, res) => {
            // decode the req. here, there will be quite a lot of stuff there. 
            console.log(`Move message received: Body is ${req.body.board}`)
            let mv = this.move(req.body.board)
            console.log(`request is ${Object.keys(req.body.board)}`)
            res.json({
                player : this.name,
                move : mv
            })
        })
        this.express.post('/gameover', (req, res, next) => {
            console.log("Aw, gameover, I was enjoying that")
            res.json({player_id: this.name, note: this.desc})
            next()
        })
        
    }
    private init(messageObject : object) {
        this.game_id = messageObject["game_id"]
        this.which_player = messageObject["which_player"] 
        console.log(`Message received By DumbRemotePlayer is: ${messageObject["message"]}`)
        return {player_id: this.name, descr: this.desc}
    }

    private move(board : [][]): number {
        let attempt = Math.floor(Math.random() * 7);
        for (let i = 0; i < 7; i++) { 
            for (let r = 0; r < board[attempt].length; r++) {
                if (board[attempt][r] == 0) {
                    return attempt 
                }
            }
            attempt = (attempt + 1) % 7
        }
        return -1 // error..
    }
    public startServer(port: number): void {
        this.express.listen(port, (err) => {
            if (err) {
              return console.log(err)
            }
          
            return console.log(`DumbRemotePlayer is listening on ${port}`)
          })
    }
}
const port = process.env.PORT || 3000
let player = new DumbRemotePlayer()
player.startServer(3000)